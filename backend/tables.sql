DROP DATABASE IF EXISTS `smartHome`;

CREATE DATABASE `smartHome` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;

-- DROP TABLE IF EXISTS `smartHome`.`admin`;
-- DROP TABLE IF EXISTS `smartHome`.`refreshToken`;
-- DROP TABLE IF EXISTS `smartHome`.`deviceLog`;
DROP TABLE IF EXISTS `smartHome`.`device`;
DROP TABLE IF EXISTS `smartHome`.`room`;
DROP TABLE IF EXISTS `smartHome`.`house`;
DROP TABLE IF EXISTS `smartHome`.`user`;

CREATE TABLE `smartHome`.`user` (
  `userId` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id người dùng',
  `fullname` varchar(100) NOT NULL COMMENT 'Họ và tên',
  `phoneNumber` varchar(50) DEFAULT NULL COMMENT 'Số điện thoại',
  `email` varchar(100) DEFAULT NULL COMMENT 'Địa chỉ email',
  `password` varchar(100) NOT NULL COMMENT 'Mật khẩu',
  `identityNumber` varchar(25) DEFAULT NULL COMMENT 'Mã căn cước công dân',
  `createdAt` datetime DEFAULT NULL COMMENT 'Thời điểm tạo',
  `updatedAt` datetime DEFAULT NULL COMMENT 'Thời điểm cập nhật gần nhất',
  PRIMARY KEY (`userId`),
  UNIQUE KEY `phoneNumber_UNIQUE` (`phoneNumber`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  CONSTRAINT `users_chk_phoneNumber_email` CHECK (((`phoneNumber` is not null) or (`email` is not null)))
);



CREATE TABLE `smartHome`.`house` (
  `houseId` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id ngôi nhà',
  `userId` bigint unsigned NOT NULL COMMENT 'Id chủ ngôi nhà',
  `houseName` varchar(256) NOT NULL COMMENT 'Định danh ngôi nhà theo người chủ',
  `createdAt` datetime DEFAULT NULL COMMENT 'Thời điểm tạo',
  `updatedAt` datetime DEFAULT NULL COMMENT 'Thời điểm sủa đổi gần nhất',
  PRIMARY KEY (`houseId`),
  UNIQUE KEY `house_user_UNIQUE` (`houseId`, `userId`),
  CONSTRAINT `fk_house_user` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE
);



CREATE TABLE `smartHome`.`room` (
  `roomId` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id căn phòng',
  `houseId` bigint unsigned NOT NULL COMMENT 'Id ngôi nhà mà căn phòng thuộc',
  `userId` bigint unsigned NOT NULL COMMENT 'Id chủ ngôi nhà mà căn phòng thuộc',
  `roomName` varchar(256) NOT NULL COMMENT 'Tên căn phòng',
  `createdAt` datetime DEFAULT NULL COMMENT 'Thời điểm tạo',
  `updatedAt` datetime DEFAULT NULL COMMENT 'Thời điểm sửa đổi gần nhất',
  PRIMARY KEY (`roomId`),
  UNIQUE KEY `room_user_UNIQUE` (`roomId`, `userId`),
  CONSTRAINT `fk_room_house_user` FOREIGN KEY (`houseId`, `userId`) REFERENCES `house` (`houseId`, `userId`) ON DELETE CASCADE
);



CREATE TABLE `smartHome`.`device` (
  `deviceId` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id thiết bị',
  `roomId` bigint unsigned NOT NULL COMMENT 'Id căn phòng mà thiết bị thuộc',
  `userId` bigint unsigned NOT NULL COMMENT 'Id chủ ngôi nhà mà thiết bị thuộc',
  `deviceMetadata` json DEFAULT NULL COMMENT 'Thông tin thiết bị',
  `deviceData` json DEFAULT NULL COMMENT 'Dữ liệu mới nhất từ thiết bị',
  `createdAt` datetime DEFAULT NULL COMMENT 'Thời điểm tạo',
  `updatedAt` datetime DEFAULT NULL COMMENT 'Thời điểm sửa đổi gần nhất',
  PRIMARY KEY (`deviceId`),
  CONSTRAINT `fk_device_room_user` FOREIGN KEY (`roomId`, `userId`) REFERENCES `room` (`roomId`, `userId`) ON DELETE CASCADE
);

DROP TABLE IF EXISTS `smartHome`.`blackToken`;

CREATE TABLE `smartHome`.`blackToken` (
  `blackToken` varchar(256) NOT NULL COMMENT 'Token mà người dùng đã đăng xuất nhưng chưa hết hạn',
  `expireAt` bigint NOT NULL COMMENT 'Thời điểm token hết hạn theo date js',
  PRIMARY KEY (`blackToken`)
);

-- CREATE TABLE `smartHome`.`deviceLog` (
--   `deviceLog` json NOT NULL COMMENT 'Dữ liệu từ thiết bị',
--   `deviceId` bigint unsigned NOT NULL COMMENT 'Id thiết bị được ghi nhật ký dữ liệu',
--   `createdAt` datetime NOT NULL COMMENT 'Thời điểm tạo',
--   PRIMARY KEY (`createdAt`, `deviceId`),
--   CONSTRAINT `fk_deviceLog_device` FOREIGN KEY (`deviceId`) REFERENCES `device` (`deviceId`) ON DELETE CASCADE
-- );


-- CREATE TABLE `smartHome`.`admin` (
--   `adminId` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id người quản trị',
--   `fullname` varchar(100) NOT NULL COMMENT 'Họ và tên',
--   `email` varchar(100) NOT NULL COMMENT 'Địa chỉ email',
--   `password` varchar(100) NOT NULL COMMENT 'Mật khẩu',
--   `createdAt` datetime DEFAULT NULL COMMENT 'Thời điểm tạo',
--   `updatedAt` datetime DEFAULT NULL COMMENT 'Thời điểm sửa đổi gần nhất',
--   PRIMARY KEY (`adminId`),
--   UNIQUE KEY `email_UNIQUE` (`email`)
-- );

-- CREATE TABLE `smartHome`.`refreshToken` (
--   `refreshToken` varchar(100) NOT NULL COMMENT 'Token để làm mới access token',
--   `userId` bigint unsigned NOT NULL COMMENT 'Id chủ tài khoản của token',
--   `expiredAt` bigint NOT NULL COMMENT 'Thời điểm token hết hạn theo date js',
--   PRIMARY KEY (`refreshToken`),
--   CONSTRAINT `fk_refreshToken_user` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE
-- );