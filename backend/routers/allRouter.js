
import verifyToken from '../middlewares/verifyTokenMiddleware.js';
import { signUp, signIn, signOut, changePassword } from "../controllers/authController.js";
import { getUser, updateUser, deleteUser } from "../controllers/userController.js";
import { postHouse, getHouse, getHouses, updateHouse, deleteHouse } from "../controllers/houseController.js";
import { postRoom, getRooms, getRoom, updateRoom, deleteRoom } from "../controllers/roomController.js";
import { getDevice, getDevices, deleteDevice, postDevice, updateDevice } from '../controllers/deviceController.js';

export default function(app) {
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Headers", "x-access-token, origin, content-type, accept");
        next();
    });

    // simple route
    app.get('/', function(req, res) {
        res.sendFile('index.html', {root: "../frontend" })
    });

    app.post("/api/auth/sign-up", signUp);

    app.post("/api/auth/sign-in", signIn);

    // under routes are verified access token
    app.use(verifyToken);
  
    app.post("/api/auth/sign-out", signOut);
  
    app.put("/api/auth/change-password", changePassword);

    app.get("/api/user", getUser);

    app.put("/api/user", updateUser);

    app.delete("/api/user", deleteUser);

    app.post("/api/house", postHouse);

    app.get("/api/house", getHouse);

    app.get("/api/houses", getHouses);

    app.put("/api/house", updateHouse);

    app.delete("/api/house", deleteHouse);

    app.post("/api/room", postRoom);

    app.get("/api/room", getRoom);

    app.get("/api/rooms", getRooms);

    app.put("/api/room", updateRoom);

    app.delete("/api/room", deleteRoom);

    app.post("/api/device", postDevice);

    app.get("/api/device", getDevice);

    app.get("/api/devices", getDevices);

    app.put("/api/device", updateDevice);

    app.delete("/api/device", deleteDevice);
}
