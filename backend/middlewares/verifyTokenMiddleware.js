import { verify } from "jsonwebtoken";
import { SECRET_KEY } from "../configs/authConfig.js";
import BlackToken from "../models/blackTokenModel.js";

export default function (req, res, next) {
	const token = req.headers["x-access-token"];

	if (!token)
		return res.json(403, { message: "No x-access-token provided!" });

	verify(token, SECRET_KEY,
		(err, decoded) => {
			if (err)
				return res.sendStatus(401);

			BlackToken.findByToken(token).then(
				(blackToken) => {
					if (blackToken) {
						return res.json(404, { message: "token did not use!" });
					}
					else {
						req.userId = decoded.id;
						req.expireAt = decoded.expireAt;
						next();
					}
				})
				.catch(err => res.json(500, { message: err.message }));
		}
	);
};