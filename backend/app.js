import express, { json, urlencoded } from 'express';
import Device from './models/deviceModel.js';
import cors from 'cors';
import mqtt from 'mqtt';

const app = express();

app.use(cors());

// parse requests of content-type - application/json
app.use(json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(urlencoded({ extended: true }));

export var deviceClientMQTT = {};
export var deviceTimes = {};
export function subscribeDevice(id) {
    deviceClientMQTT[id] = mqtt.connect('mqtt://test.mosquitto.org:1883');

    deviceClientMQTT[id].on('connect', function () {
        deviceClientMQTT[id].subscribe(`device/${id}`, {qos: 1}, function (err) {
            if (!err) {
                console.log(`${id} subscribe device/${id}: DONE!`);
                deviceTimes[id] = Date.now();
                deviceClientMQTT[id].on('message', function (topic, message) {
                    try {
                        deviceTimes[id] = Date.now();
                        message = JSON.parse(message);
                        if (message.isDevice != null && message.isDevice != undefined) {
                            if (message.isDevice == 1 || message.isDevice == true ) {
                                message.data.status = (message.data.status == 1 || message.data.status) ? true : false;
                                if (message.data.status) {
                                    setTimeout(() => {
                                        if (deviceTimes[id] && message.data.status && deviceTimes[id] + 10000 < Date.now()) {
                                            Device.editDataById(id, JSON.stringify({status: false}));
                                        }
                                    }, 600000)
                                }
                                console.log(`${id}: ${JSON.stringify(message.data)}`);
                                Device.editDataById(id, JSON.stringify(message.data))//.then((x) => console.log(x));
                            }
                        }
                    } catch (error) {
                        // console.error(error);
                    }
                })
                setTimeout(() => {
                    if (deviceTimes[id] && deviceTimes[id] + 10000 < Date.now()) {
                        Device.editDataById(id, JSON.stringify({status: false}));
                    }
                }, 600000)
            }
        });
    })

    
}
Device.findIdAll().then((deviceIds) => {
    if (deviceIds) for (const id of deviceIds) subscribeDevice(id);
});

app.set("view engine", "ejs");
app.use(express.static("../frontend"));

// routes
import allRoute from './routers/allRouter.js';
allRoute(app);

export default app;