import { connection } from '../configs/dbConfig.js';

const TABLE = 'device';

export default class Device
{
    constructor(roomId, deviceMetadata, deviceData) {
        this.roomId= roomId;
        this.deviceMetadata = deviceMetadata;
        this.deviceData = deviceData;
        /* 
           `deviceId` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id thiết bị',
            `roomId` bigint unsigned NOT NULL COMMENT 'Id căn phòng mà thiết bị thuộc',
            `userId` bigint unsigned NOT NULL COMMENT 'Id chủ ngôi nhà mà thiết bị thuộc',
            `deviceMetadata` json DEFAULT NULL COMMENT 'Thông tin thiết bị',
            `deviceData` json DEFAULT NULL COMMENT 'Dữ liệu mới nhất từ thiết bị',
            `createdAt` datetime DEFAULT NULL COMMENT 'Thời điểm tạo',
            `updatedAt` datetime DEFAULT NULL COMMENT 'Thời điểm sửa đổi gần nhất',
         */
    }

    static async create(userId, device) {
        return new Promise((resolve, reject) => {
            connection.query(
                `INSERT INTO ${TABLE} (roomId, userId, deviceMetadata, deviceData, createdAt, updatedAt) VALUES(?, ?, ?, ?, NOW(), NOW())`, 
                [device.roomId, userId, device.deviceMetadata, device.deviceData], 
                (error, result) => error ? reject(error) : resolve(result.insertId)
            );    
        });
    }

    static async findIdAll() {
        return new Promise((resolve, reject) => {
            connection.query(
                `SELECT deviceId FROM ${TABLE}`,  
                (error, result) => error ? reject(error) : resolve(result.length ? result.map(device => device.deviceId) : null) 
            ); 
        });
    }

    static async findByRoomId(userId, roomId) {
        return new Promise((resolve, reject) => {
            connection.query(
                `SELECT * FROM ${TABLE} WHERE userId = ? AND roomId = ?`, 
                [userId, roomId], 
                (error, result) => error ? reject(error) : resolve(result.length ? result : null) 
            ); 
        });
    }

    static async findById(userId, id) {
        return new Promise((resolve, reject) => {
            connection.query(
                `SELECT * FROM ${TABLE} WHERE ${TABLE}Id = ? AND userId = ?`, 
                [id, userId], 
                (error, result) => error ? reject(error) : resolve(result.length ? result[0] : null) 
            ); 
        });
    }

    static async editById(userId, id, properties) {

        const columns = ['roomId', 'deviceMetadata', 'deviceData'];
        
        let sql = `UPDATE ${TABLE} SET `;
        let endColumns = [];

        for (const column of columns) {
            if (column in properties) {
                sql += column + '=?, ';
                endColumns.push(properties[column] ? properties[column] : null);
            }
        }

        sql += `updatedAt = NOW() WHERE ${TABLE}Id = ? AND userId = ?`;
        endColumns.push(id);
        endColumns.push(userId);
        
        return new Promise((resolve, reject) => {
            connection.query(
                sql, 
                endColumns, 
                (error, result) => error ? reject(error) : resolve(result.affectedRows ? id : null)  
            );
        });
    }

    static async editDataById(id, data) {
        return new Promise((_resolve, _reject) => {
            new Promise((resolve, reject) => {
                connection.query(
                    `SELECT ${TABLE}Data FROM ${TABLE} WHERE ${TABLE}Id = ?`, 
                    [id], 
                    (error, result) => error ? reject(error) : resolve(result.length ? result[0].deviceData : null)  
                );
            }).then(oldData => {
                data = JSON.parse(data);
                oldData = JSON.parse(oldData);
                for (const column in oldData) {
                    if (! (column in data) ) {
                        data[column] = oldData[column];
                    }
                }
                data = JSON.stringify(data);
                //console.log(data);
    
                connection.query(
                    `UPDATE ${TABLE} SET ${TABLE}Data = ?, updatedAt = NOW() WHERE ${TABLE}Id = ?`, 
                    [data, id], 
                    (error, result) => error ? _reject(error) : _resolve(result.affectedRows ? id : null)  
                );
            }).catch(error => _reject(error));
        })
    }

    static async removeById(userId, id) {
        return new Promise((resolve, reject) => {
            connection.query(
                `DELETE FROM ${TABLE} WHERE ${TABLE}Id = ? AND userId = ?`, 
                [id, userId], 
                (error, result) => error ? reject(error) : resolve(result.affectedRows ? id : null) 
            );
        });
    }
}
