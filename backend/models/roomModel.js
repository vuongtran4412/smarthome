import { connection } from '../configs/dbConfig.js';

const TABLE = 'room';

export default class Room
{
    constructor(houseId, roomName) {
        this.houseId = houseId;
        this.roomName = roomName;
        /* 
           `roomId` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id căn phòng',
            `houseId` bigint unsigned NOT NULL COMMENT 'Id ngôi nhà mà căn phòng thuộc',
            `userId` bigint unsigned NOT NULL COMMENT 'Id chủ ngôi nhà mà căn phòng thuộc',
            `roomName` varchar(256) NOT NULL COMMENT 'Tên căn phòng',
            `createdAt` datetime DEFAULT NULL COMMENT 'Thời điểm tạo',
            `updatedAt` datetime DEFAULT NULL COMMENT 'Thời điểm sửa đổi gần nhất',
         */
    }

    static async create(userId, room) {
        return new Promise((resolve, reject) => {
            connection.query(
                `INSERT INTO ${TABLE} (houseId, userId, roomName, createdAt, updatedAt) VALUES(?, ?, ?, NOW(), NOW())`, 
                [room.houseId, userId, room.roomName], 
                (error, result) => error ? reject(error) : resolve(result.insertId)
            );    
        });
    }

    static async findByHouseId(userId, houseId) {
        return new Promise((resolve, reject) => {
            connection.query(
                `SELECT * FROM ${TABLE} WHERE userId = ? AND houseId = ?`, 
                [userId, houseId], 
                (error, result) => error ? reject(error) : resolve(result.length ? result : null) 
            ); 
        });
    }

    static async findById(userId, id) {
        return new Promise((resolve, reject) => {
            connection.query(
                `SELECT * FROM ${TABLE} WHERE ${TABLE}Id = ? AND userId = ?`, 
                [id, userId], 
                (error, result) => error ? reject(error) : resolve(result.length ? result[0] : null) 
            ); 
        });
    }

    static async editById(userId, id, properties) {

        const columns = ['houseId', 'roomName'];
        
        let sql = `UPDATE ${TABLE} SET `;
        let endColumns = [];

        for (const column of columns) {
            if (column in properties) {
                sql += column + '=?, ';
                endColumns.push(properties[column] ? properties[column] : null);
            }
        }

        sql += `updatedAt = NOW() WHERE ${TABLE}Id = ? AND userId = ?`;
        endColumns.push(id);
        endColumns.push(userId);
        
        return new Promise((resolve, reject) => {
            connection.query(
                sql, 
                endColumns, 
                (error, result) => error ? reject(error) : resolve(result.affectedRows ? id : null)  
            );
        });
    }

    static async removeById(userId, id) {
        return new Promise((resolve, reject) => {
            connection.query(
                `DELETE FROM ${TABLE} WHERE ${TABLE}Id = ? AND userId = ?`, 
                [id, userId], 
                (error, result) => error ? reject(error) : resolve(result.affectedRows ? id : null) 
            );
        });
    }
}
