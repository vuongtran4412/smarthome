import { connection } from '../configs/dbConfig.js';

const TABLE = 'house';

export default class House
{
    constructor(houseName) {
        this.houseName = houseName;
        /* 
            `houseId` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id ngôi nhà',
            `userId` bigint unsigned NOT NULL COMMENT 'Id chủ ngôi nhà',
            `houseName` varchar(256) NOT NULL COMMENT 'Định danh ngôi nhà theo người chủ',
            `createdAt` datetime DEFAULT NULL COMMENT 'Thời điểm tạo',
            `updatedAt` datetime DEFAULT NULL COMMENT 'Thời điểm sủa đổi gần nhất',
         */
    }

    static async create(userId, house) {
        return new Promise((resolve, reject) => {
            connection.query(
                `INSERT INTO ${TABLE} (userId, houseName, createdAt, updatedAt) VALUES(?, ?, NOW(), NOW())`, 
                [userId, house.houseName], 
                (error, result) => error ? reject(error) : resolve(result.insertId)
            );    
        });
    }

    static async findByUserId(userId) {
        return new Promise((resolve, reject) => {
            connection.query(
                `SELECT * FROM ${TABLE} WHERE userId = ?`, 
                [userId], 
                (error, result) => error ? reject(error) : resolve(result.length ? result : null) 
            ); 
        });
    }

    static async findById(userId, id) {
        return new Promise((resolve, reject) => {
            connection.query(
                `SELECT * FROM ${TABLE} WHERE ${TABLE}Id = ? AND userId = ?`, 
                [id, userId], 
                (error, result) => error ? reject(error) : resolve(result.length ? result[0] : null) 
            ); 
        });
    }

    static async editById(userId, id, properties) {

        const columns = ['houseName'];
        
        let sql = `UPDATE ${TABLE} SET `;
        let endColumns = [];

        for (const column of columns) {
            if (column in properties) {
                sql += column + '=?, ';
                endColumns.push(properties[column] ? properties[column] : null);
            }
        }

        sql += `updatedAt = NOW() WHERE ${TABLE}Id = ? AND userId = ?`;
        endColumns.push(id);
        endColumns.push(userId);
        
        return new Promise((resolve, reject) => {
            connection.query(
                sql, 
                endColumns, 
                (error, result) => error ? reject(error) : resolve(result.affectedRows ? id : null)  
            );
        });
    }

    static async removeById(userId, id) {
        return new Promise((resolve, reject) => {
            connection.query(
                `DELETE FROM ${TABLE} WHERE ${TABLE}Id = ? AND userId = ?`, 
                [id, userId], 
                (error, result) => error ? reject(error) : resolve(result.affectedRows ? id : null) 
            );
        });
    }
}
