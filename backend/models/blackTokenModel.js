import { connection } from '../configs/dbConfig.js';

const TABLE = 'blackToken';

export default class blackToken
{
    constructor(blackToken, expireAt) {
        this.blackToken = blackToken;
        this.expireAt = expireAt;
        /* 
            `blackToken` varchar(100) NOT NULL COMMENT 'Token mà người dùng đã đăng xuất nhưng chưa hết hạn',
            `expireAt` bigint NOT NULL COMMENT 'Thời điểm token hết hạn theo date js',
         */
    }

    static async create(blackToken) {
        return new Promise((resolve, reject) => {
            connection.query(
                `INSERT INTO ${TABLE} VALUES(?, ?)`, 
                [blackToken.blackToken, blackToken.expireAt], 
                (error, result) => error ? reject(error) : resolve()
            );    
        });
    }

    static async findByToken(token) {
        return new Promise((resolve, reject) => {
            connection.query(
                `SELECT * FROM ${TABLE} WHERE ${TABLE} = ?`, 
                [token], 
                (error, result) => error ? reject(error) : resolve(result.length ? result[0] : null) 
            ); 
        });
    }

    static async removeExpiredBlackToken() {
        return new Promise((resolve, reject) => {
            connection.query(
                `DELETE FROM ${TABLE} WHERE expireAt < ?`, 
                [Date.now()], 
                (error, result) => error ? reject(error) : resolve() 
            );
        });
    }
}