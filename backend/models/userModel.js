import { connection } from '../configs/dbConfig.js';

const TABLE = 'user';

export default class User
{
    constructor(fullname, phoneNumber, email, password, identityNumber) {
        this.fullname = fullname;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.password = password;
        this.identityNumber = identityNumber;
        /* 
            `userId` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id người dùng',
            `fullname` varchar(100) NOT NULL COMMENT 'Họ và tên',
            `phoneNumber` varchar(50) DEFAULT NULL COMMENT 'Số điện thoại',
            `email` varchar(100) DEFAULT NULL COMMENT 'Địa chỉ email',
            `password` varchar(100) NOT NULL COMMENT 'Mật khẩu',
            `identityNumber` varchar(25) DEFAULT NULL COMMENT 'Mã căn cước công dân',
            `createdAt` datetime DEFAULT NULL COMMENT 'Thời điểm tạo',
            `updatedAt` datetime DEFAULT NULL COMMENT 'Thời điểm cập nhật gần nhất',
         */
    }

    static async create(user) {
        return new Promise((resolve, reject) => {
            connection.query(
                `INSERT INTO ${TABLE} (fullname, phoneNumber, email, password, identityNumber, createdAt, updatedAt) VALUES(?, ?, ?, ?, ?, NOW(), NOW())`, 
                [user.fullname, user.phoneNumber, user.email, user.password, user.identityNumber], 
                (error, result) => error ? reject(error) : resolve(result.insertId)
            );    
        });
    }

    static async findByEmail(email) {
        return new Promise((resolve, reject) => {
            connection.query(
                `SELECT * FROM ${TABLE} WHERE email = ?`, 
                [email], 
                (error, result) => error ? reject(error) : resolve(result.length ? result[0] : null) 
            ); 
        });
    }

    static async findByPhoneNumber(phoneNumber) {
        return new Promise((resolve, reject) => {
            connection.query(
                `SELECT * FROM ${TABLE} WHERE phoneNumber = ?`, 
                [phoneNumber], 
                (error, result) => error ? reject(error) : resolve(result.length ? result[0] : null) 
            ); 
        });
    }

    static async findById(id) {
        return new Promise((resolve, reject) => {
            connection.query(
                `SELECT * FROM ${TABLE} WHERE ${TABLE}Id = ?`, 
                [id], 
                (error, result) => error ? reject(error) : resolve(result.length ? result[0] : null) 
            ); 
        });
    }

    static async editById(id, properties) {

        const columns = ['fullname', 'phoneNumber', 'email', 'password', 'identityNumber'];
        
        let sql = `UPDATE ${TABLE} SET `;
        let endColumns = [];

        for (const column of columns) {
            if (column in properties) {
                sql += column + '=?, ';
                endColumns.push(properties[column] ? properties[column] : null);
            }
        }

        sql += `updatedAt = NOW() WHERE ${TABLE}Id = ?`;
        endColumns.push(id);
        
        return new Promise((resolve, reject) => {
            connection.query(
                sql, 
                endColumns, 
                (error, result) => error ? reject(error) : resolve(result.affectedRows ? id : null)  
            );
        });
    }

    static async removeById(id) {
        return new Promise((resolve, reject) => {
            connection.query(
                `DELETE FROM ${TABLE} WHERE ${TABLE}Id = ?`, 
                [id], 
                (error, result) => error ? reject(error) : resolve(result.affectedRows ? id : null) 
            );
        });
    }
}
