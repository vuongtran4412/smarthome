import app from './app.js';

const PORT = 8080;
const HOST = 'localhost';

app.listen(PORT, HOST, () => {
    console.log(`Server is running on: http://${HOST}:${PORT}`);
});