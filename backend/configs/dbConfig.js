import { createConnection } from 'mysql';

const HOST = "localhost";
const USER = "root";
const PASSWORD = "password";
const DB = "smartHome";

export const connection = createConnection({
	host: HOST,
	user: USER,
	password: PASSWORD,
	database: DB
});

connection.connect(
	(err) => {
		if (err)
			console.log(err.message);
		else
			console.log('Database connected')
	}
);