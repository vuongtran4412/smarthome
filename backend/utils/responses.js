export function responseObj(res, obj) {
    if (!obj) 
        return res.sendStatus(404);
    delete obj.password;
    return res.status(200).json(obj);
}

export function responseError500(res, err) {
    return res.status(500).json({ message: err.message });
}