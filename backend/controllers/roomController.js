
import Room from '../models/roomModel.js';
import { responseObj, responseError500 } from '../utils/responses.js';

export async function postRoom(req, res) 
{
  const { houseId, roomName } = req.body;

  if (!roomName || (houseId == null || houseId == undefined)) 
    return res.sendStatus(400);
  
  const room = new Room(houseId, roomName.trim());

  Room.create(req.userId, room)
        .then(id => res.status(200).json({roomId: id}))
        .catch(err => responseError500(res, err));
};

export async function getRooms(req, res) {

    const houseId = req.body.houseId ?? req.query.houseId;

    if (houseId == null || houseId == undefined) 
        return res.sendStatus(404);

    Room.findByHouseId(req.userId, houseId)
        .then(obj => responseObj(res, obj))
        .catch(err => responseError500(res, err));
}

export async function getRoom(req, res) {

    const roomId = req.body.roomId ?? req.query.roomId;

    if (roomId == undefined || roomId == null) 
        return res.sendStatus(404);

    Room.findById(req.userId, roomId)
        .then(obj => responseObj(res, obj))
        .catch(err => responseError500(res, err));
}

export async function updateRoom(req, res) {
    
    const { roomId, houseId, roomName } = req.body;

    if (roomId == undefined || roomId == null) 
    return res.sendStatus(404);

    let room = {};
    if (houseId != undefined) room.houseId = houseId;
    if (roomName != undefined) room.roomName = roomName.trim();

    Room.editById(req.userId, roomId, room)
        .then(id => {
            if (id == null || id == undefined) {
                res.sendStatus(404);
            }
            else {
                res.status(200).json({roomId: id});
            }
        })
        .catch(err => responseError500(res, err));
}

export async function deleteRoom(req, res) {
    
    const roomId = req.body.roomId ?? req.query.roomId;

    if (roomId == undefined || roomId == null) 
        return res.sendStatus(404);

        Room.removeById(req.userId, roomId)
        .then(id => {
            if (id == null || id == undefined) {
                res.sendStatus(404);
            }
            else {
                res.status(200).json({roomId: id});
            }
        })
        .catch(err => responseError500(res, err));
}