
import House from '../models/houseModel.js';
import { responseObj, responseError500 } from '../utils/responses.js';

export async function postHouse(req, res) 
{
  const { houseName } = req.body;

  if (!houseName) 
    return res.sendStatus(400);
  
  const house = new House(houseName.trim());

  House.create(req.userId, house)
        .then(id => res.status(200).json({houseId: id}))
        .catch(err => responseError500(res, err));
};

export async function getHouses(req, res) {

  House.findByUserId(req.userId)
        .then(obj => responseObj(res, obj))
        .catch(err => responseError500(res, err));
}

export async function getHouse(req, res) {

    const houseId = req.body.houseId ?? req.query.houseId;

    if (houseId == undefined || houseId == null) 
        return res.sendStatus(404);

    House.findById(req.userId, houseId)
        .then(obj => responseObj(res, obj))
        .catch(err => responseError500(res, err));
}

export async function updateHouse(req, res) {
    const { houseId, houseName } = req.body;

    if (houseId == undefined || houseId == null) 
    return res.sendStatus(404);

    let house = {};
    if (houseName != undefined) house.houseName = houseName.trim();

    House.editById(req.userId, houseId, house)
        .then(id => {
            if (id == null || id == undefined) {
                res.sendStatus(404);
            }
            else {
                res.status(200).json({houseId: id});
            }
        })
        .catch(err => responseError500(res, err));
}

export async function deleteHouse(req, res) {
    
    const houseId = req.body.houseId ?? req.query.houseId;

    if (houseId == undefined || houseId == null) 
        return res.sendStatus(404);

        House.removeById(req.userId, houseId)
        .then(id => {
            if (id == null || id == undefined) {
                res.sendStatus(404);
            }
            else {
                res.status(200).json({houseId: id});
            }
        })
        .catch(err => responseError500(res, err));
}