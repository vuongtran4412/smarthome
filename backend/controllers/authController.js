import { SECRET_KEY, SLICE, TIME_EXPIRED } from "../configs/authConfig.js";
import User from "../models/userModel.js";
import BlackToken from "../models/blackTokenModel.js";
import JWT from "jsonwebtoken";
import { createHash } from "crypto";
import { responseError500 } from '../utils/responses.js';

export async function signUp(req, res) 
{
  const { fullname, phoneNumber, email, password, identityNumber } = req.body;
  
  if (!password || !(phoneNumber || email) || !fullname) 
    return res.sendStatus(400);

  const hashedPassword = hashPassword(password.trim());

  const user = new User(fullname.trim(), phoneNumber?.trim(), email?.trim(), hashedPassword, identityNumber?.trim());

  User.create(user)
      .then(id => res.status(200).json({userId: id}))
      .catch(err => responseError500(res, err));
};

export async function signIn(req, res) 
{
  const { phoneNumber, email, password } = req.body;

  if (!(email || phoneNumber) || !password) 
    return res.sendStatus(400);

  if (email) {
    User.findByEmail(email.trim())
        .then(user => ans(user, password, res))
        .catch(err => responseError500(res, err));
  }
  else if (phoneNumber) {
    User.findByPhoneNumber(phoneNumber.trim())
        .then(user => ans(user, password, res))
        .catch(err => responseError500(res, err));
  }
};

export async function signOut(req, res) {
    // đưa access token chưa hết hạn vào black_tokens
    const blackToken = new BlackToken(req.headers["x-access-token"], req.expireAt);
    BlackToken.create(blackToken)
              .then(() => res.status(200).json({ message: 'logout success!'}))
              .catch(err => responseError500(res, err));
}

export async function changePassword(req, res) {
  const { password, newPassword } = req.body;

  if (!password || !newPassword) 
    return res.sendStatus(400);

  User.findById(req.userId)
    .then(
      user => {
        if (!user) 
          return res.sendStatus(404);
        
        if (! comparePassword(password.trim(), user.password))
          return res.sendStatus(401);

        const hashedNewPassword = hashPassword(newPassword.trim());

        User.editById(req.userId, {password: hashedNewPassword})
        .then(
          id => {
            if (id == null || id == undefined) {
                res.sendStatus(404);
            }
            else {
                res.status(200).json({userId: id});
            }
          }
        )
        .catch(err => responseError500(res, err));
      }
    )
    .catch(err => responseError500(res, err));
}

function ans(user, password, res) 
{
  if (!user)
    return res.sendStatus(404);

  if (! comparePassword(password?.trim(), user.password))
    return res.status(401).json({accessToken: null, message: "Invalid Password!"});

  BlackToken.removeExpiredBlackToken().then(() => {
    res.status(200).json(
      {
        id: user.userId,
        accessToken: JWT.sign(
          { 
            id: user.userId, 
            expireAt: Date.now() + TIME_EXPIRED 
          }, 
          SECRET_KEY, 
          {expiresIn: TIME_EXPIRED}
        )
      }
    );
  })
  .catch(err => res.json(500, { message: err.message }));
}

function hashPassword(password) {
  const salt = Math.round(Math.random() * 3333);
  const hashedPassword = createHash('sha256').update(password + salt).digest('hex');
  return hashedPassword.slice(0, SLICE) + salt + hashedPassword.slice(SLICE);
}

function comparePassword(password, storedPassword) {
  const afterSlice = storedPassword.length - 64 + SLICE;
  const salt = storedPassword.slice(SLICE, afterSlice);
  const hashedPassword = storedPassword.slice(0, SLICE) + storedPassword.slice(afterSlice);
  const tryHashPassword = createHash('sha256').update(password + salt).digest('hex');
  return hashedPassword == tryHashPassword;
}