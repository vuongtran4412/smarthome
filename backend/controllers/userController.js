
import User from '../models/userModel.js';
import { responseObj, responseError500 } from '../utils/responses.js';

export async function getUser(req, res) {

    User.findById(req.userId)
        .then(obj => responseObj(res, obj))
        .catch(err => responseError500(res, err));
}

export async function updateUser(req, res) {
    const { fullname, phoneNumber, email, identityNumber } = req.body;

    let user = {};
    if (fullname != undefined) user.fullname = fullname.trim();
    if (phoneNumber != undefined) user.phoneNumber = phoneNumber.trim();
    if (email != undefined) user.email = email.trim();
    if (identityNumber != undefined) user.identityNumber = identityNumber.trim();

    User.editById(req.userId, user)
        .then(
            id => {
                if (id == null || id == undefined) {
                    res.sendStatus(404);
                }
                else {
                    res.status(200).json({userId: id});
                }
            }
        )
        .catch(err => responseError500(res, err));
}

export async function deleteUser(req, res) {
    User.removeById(req.userId)
        .then(
            id => {
                if (id == null || id == undefined) {
                    res.sendStatus(404);
                }
                else {
                    res.status(200).json({userId: id});
                }
            }
        )
        .catch(err => responseError500(res, err));
}