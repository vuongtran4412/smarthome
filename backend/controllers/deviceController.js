
import Device from '../models/deviceModel.js';
import { deviceTimes, deviceClientMQTT, subscribeDevice } from '../app.js';
import { responseObj, responseError500 } from '../utils/responses.js';

export async function postDevice(req, res) 
{
  const { roomId, deviceMetadata, deviceData } = req.body;

  if (!deviceMetadata || (roomId == null || roomId == undefined)) 
    return res.sendStatus(400);

  const device = new Device(
    roomId, 
    JSON.stringify(deviceMetadata), 
    deviceData ? JSON.stringify(deviceData) : null
  );

  Device.create(req.userId, device)
        .then(id => {
            res.status(200).json({deviceId: id});
            
            subscribeDevice(id);
        })
        .catch(err => responseError500(res, err));
};

export async function getDevices(req, res) {

    const roomId = req.body.roomId ?? req.query.roomId;

    if (roomId == null || roomId == undefined) 
        return res.sendStatus(404);

    Device.findByRoomId(req.userId, roomId)
        .then(obj => responseObj(res, obj))
        .catch(err => responseError500(res, err));
}

export async function getDevice(req, res) {

    const deviceId = req.body.deviceId ?? req.query.deviceId;

    if (deviceId == undefined || deviceId == null) 
        return res.sendStatus(404);

    Device.findById(req.userId, deviceId)
        .then(obj => responseObj(res, obj))
        .catch(err => responseError500(res, err));
}

export async function updateDevice(req, res) {
    const { deviceId, roomId, deviceMetadata, deviceData } = req.body;

    if (deviceId == undefined || deviceId == null) 
    return res.sendStatus(404);

    let device = {};
    if (roomId != undefined) device.roomId = roomId;
    if (deviceMetadata != undefined) device.deviceMetadata = JSON.stringify(deviceMetadata);
        
    Device.editById(req.userId, deviceId, device)
            .then(id => {
                if (id == null || id == undefined) {
                    res.sendStatus(404);
                }
                else {
                    if (deviceData != undefined) {
                        deviceData.status = (deviceData.status == 1 || deviceData.status == true) ? true : false;
                        Device.editDataById(deviceId, JSON.stringify(deviceData)).then(() => {
                            deviceData.status = (deviceData.status == 1 || deviceData.status == true) ? 1 : 0;
                    
                            deviceClientMQTT[id].publish(`device/${id}`, JSON.stringify({isDevice: 0, data: deviceData}), { qos: 1 });
                            
                            res.status(200).json({deviceId: id});

                        }).catch(err => responseError500(res, err));
                    }
                    else {
                        res.status(200).json({deviceId: id})
                    }
                }
            })
            .catch(err => responseError500(res, err));
}

export async function deleteDevice(req, res) {
    
    const deviceId = req.body.deviceId ?? req.query.deviceId;

    if (deviceId == undefined || deviceId == null) 
        return res.sendStatus(404);

    Device.removeById(req.userId, deviceId)
        .then(id => {
            if (id == null || id == undefined) {
                res.sendStatus(404);
            }
            else {
                res.status(200).json({deviceId: id})

                deviceClientMQTT[id].end();
                delete deviceClientMQTT[id];
                delete deviceTimes[id];
            }
        })
        .catch(err => responseError500(res, err));
}