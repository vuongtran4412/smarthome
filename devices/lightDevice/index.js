import mqtt from 'mqtt';

var args = process.argv.slice(2);
const id = args[0]; 
var status = true;
const client  = mqtt.connect('mqtt://test.mosquitto.org:1883');

client.on('connect', function () {
    client.subscribe(`device/${id}`, function (err) { 
      if (!err) {
        setInterval(
            () => {
                if (status) {
                    client.publish(`device/${id}`, JSON.stringify({isDevice: 1, data: {status: 1} }), { qos: 1 });
                    console.log(`device/${id} : publish...`);
                }
            }
            , 5000 * 1
        );
      }
    });
})

client.on('message', function (topic, message) {
    try {
        message = JSON.parse(message);
        if (message.isDevice != null &&  message.isDevice != undefined) {
            if (message.isDevice == 0 || message.isDevice == false) {
                // console.log(`${topic} : ${message}`);
                status = (message.data.status == 1 || message.data.status) ? 1 : 0;
                console.log(`${topic} : ${status == 1 ? "bật" : "tắt"}`);
            }
        }
    } catch (error) {
        // console.error(error);
    }
})