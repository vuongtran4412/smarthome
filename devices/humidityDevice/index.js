import mqtt from 'mqtt';

var args = process.argv.slice(2);
const id = args[0]; 
var status = true;
const humidities = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95];
const client  = mqtt.connect('mqtt://test.mosquitto.org:1883');

client.on('connect', function () {
    client.subscribe(`device/${id}`, function (err) { 
      if (!err) {
        setInterval(
            () => {
                if (status) {
                    client.publish(`device/${id}`, JSON.stringify({isDevice: true, data: {status: true, humidity: humidities[Math.ceil(Math.random() * (humidities.length - 1))]}}), { qos: 1 });
                    console.log(`device/${id} : publish...`);
                }
            }
            , 5000 * 1
        );
      }
    });
})

client.on('message', function (topic, message) {
    try {
        message = JSON.parse(message);
        if (message.isDevice != null &&  message.isDevice != undefined) {
            if (message.isDevice == 0 || message.isDevice == false) {
                // console.log(`${topic} : ${message}`);
                status = (message.data.status == 1 || message.data.status) ? 1 : 0;
                console.log(`${topic} : ${status == 1 ? "bật" : "tắt"}`);
            }
        }
    } catch (error) {
        // console.error(error);
    }
})